/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_gizmos.cpp
 * Author: frankiezafe
 * 
 * Created on January 9, 2019, 11:36 AM
 */

#include "futari_gizmos.h"

// +++++++++++++++ WIND +++++++++++++++

FutariWindGizmoPlugin::FutariWindGizmoPlugin() {

    Color gizmo_color = EDITOR_DEF("editors/3d_gizmos/gizmo_colors/futari_wind", Color(0.0, 1.0, 1.0, 0.7));
    create_material("futari_wind", gizmo_color);
    create_handle_material("futari_wind_handles");

}

bool FutariWindGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariWind>(p_spatial);
}

String FutariWindGizmoPlugin::get_name() const {
    return "FutariWind";
}

bool FutariWindGizmoPlugin::is_selectable_when_hidden() const {
    return true;
}

void FutariWindGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

    std::cout << "FutariWindGizmoPlugin::redraw" << std::endl;

    FutariWind *wind = Object::cast_to<FutariWind>(p_gizmo->get_spatial_node());

    p_gizmo->clear();

    Ref<Material> material = get_material("futari_wind", p_gizmo);
    Ref<Material> handles_material = get_material("futari_wind_handles");

    const int arrow_points = 7;
    const float arrow_length = 1.5;
    float m = wind->get_strength();
    if (m > 1) {
        m = Math::sqrt(m);
    }

    Vector3 arrow[arrow_points] = {
        Vector3(0, 0, -1 * m),
        Vector3(0, 0.5 * m, 0),
        Vector3(0, 0.3 * m, 0),
        Vector3(0, 0.3 * m, arrow_length * m),
        Vector3(0, -0.3 * m, arrow_length * m),
        Vector3(0, -0.3 * m, 0),
        Vector3(0, -0.5 * m, 0)
    };

    int arrow_sides = 2;

    Vector<Vector3> lines;

    for (int i = 0; i < arrow_sides; i++) {
        for (int j = 0; j < arrow_points; j++) {
            Basis ma(Vector3(0, 0, 1), Math_PI * i / arrow_sides);

            Vector3 v1 = arrow[j] - Vector3(0, 0, arrow_length * m);
            Vector3 v2 = arrow[(j + 1) % arrow_points] - Vector3(0, 0, arrow_length * m);

            lines.push_back(ma.xform(v1));
            lines.push_back(ma.xform(v2));
        }
    }

    // range sphere
    const float radius = wind->get_range();
    const float agap = (Math_PI / 16);
    float a = 0;
    Vector<Vector2> circle;
    for (int i = 0; i < 32; ++i) {
        circle.push_back(Vector2(Math::cos(a + agap) * radius, Math::sin(a + agap) * radius));
        a += agap;
    }
    for (int i = 0; i < 3; ++i) {
        for (int j = 0, k = 1; j < 32; ++j, ++k) {
            k %= 32;
            Vector2 v1 = circle[j];
            Vector2 v2 = circle[k];
            switch (i) {
                case 0: // floor
                    lines.push_back(Vector3(v1.x, 0, v1.y));
                    lines.push_back(Vector3(v2.x, 0, v2.y));
                    break;
                case 1: // front wall
                    lines.push_back(Vector3(v1.x, v1.y, 0));
                    lines.push_back(Vector3(v2.x, v2.y, 0));
                    break;
                case 2: // side wall
                    lines.push_back(Vector3(0, v1.x, v1.y));
                    lines.push_back(Vector3(0, v2.x, v2.y));
                    break;
                default:
                    break;
            }
        }
    }

    p_gizmo->add_lines(lines, material);

    //    p_gizmo->add_unscaled_billboard(icon, 0.05);

}

String FutariWindGizmoPlugin::get_handle_name(const EditorSpatialGizmo *p_gizmo, int p_idx) const {
    if (p_idx == 0)
        return "Range";
    else
        return "Strength";
}

Variant FutariWindGizmoPlugin::get_handle_value(EditorSpatialGizmo *p_gizmo, int p_idx) const {

    FutariWind *wind = Object::cast_to<FutariWind>(p_gizmo->get_spatial_node());
    if (p_idx == 0)
        return wind->get_range();
    if (p_idx == 1)
        return wind->get_strength();
    return Variant();

}

void FutariWindGizmoPlugin::set_handle(EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point) {

    FutariWind *wind = Object::cast_to<FutariWind>(p_gizmo->get_spatial_node());
    Transform gt = wind->get_global_transform();
    gt.orthonormalize();
    Transform gi = gt.affine_inverse();

    Vector3 ray_from = p_camera->project_ray_origin(p_point);
    Vector3 ray_dir = p_camera->project_ray_normal(p_point);

    Vector3 s[2] = {gi.xform(ray_from), gi.xform(ray_from + ray_dir * 4096)};

    if (p_idx == 0) {

    } else if (p_idx == 1) {

    }

}

void FutariWindGizmoPlugin::commit_handle(EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel) {

    FutariWind *wind = Object::cast_to<FutariWind>(p_gizmo->get_spatial_node());

    if (p_cancel) {

        if (p_idx == 0) {
            wind->set_range(p_restore);
        } else if (p_idx == 0) {
            wind->set_strength(p_restore);
        }

    } else if (p_idx == 0) {

        //        UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
        //        ur->create_action(TTR("Change Light Radius"));
        //        ur->add_do_method(light, "set_param", Light::PARAM_RANGE, light->get_param(Light::PARAM_RANGE));
        //        ur->add_undo_method(light, "set_param", Light::PARAM_RANGE, p_restore);
        //        ur->commit_action();

    } else if (p_idx == 1) {

        //        UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
        //        ur->create_action(TTR("Change Light Radius"));
        //        ur->add_do_method(light, "set_param", Light::PARAM_SPOT_ANGLE, light->get_param(Light::PARAM_SPOT_ANGLE));
        //        ur->add_undo_method(light, "set_param", Light::PARAM_SPOT_ANGLE, p_restore);
        //        ur->commit_action();

    }
}

// +++++++++++++++ ATTRACTOR +++++++++++++++

FutariAttractorGizmoPlugin::FutariAttractorGizmoPlugin() {

    Color gizmo_color = EDITOR_DEF("editors/3d_gizmos/gizmo_colors/futari_attractor", Color(1.0, 0.0, 1.0, 0.7));
    create_material("futari_attractor", gizmo_color);
    create_handle_material("futari_attractor_handles");

}

bool FutariAttractorGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariAttractor>(p_spatial);
}

String FutariAttractorGizmoPlugin::get_name() const {
    return "FutariAttractor";
}

bool FutariAttractorGizmoPlugin::is_selectable_when_hidden() const {
    return true;
}

void FutariAttractorGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

    std::cout << "FutariAttractorGizmoPlugin::redraw" << std::endl;

    FutariAttractor *attractor = Object::cast_to<FutariAttractor>(p_gizmo->get_spatial_node());

    p_gizmo->clear();

    Ref<Material> material = get_material("futari_attractor", p_gizmo);
    Ref<Material> handles_material = get_material("futari_attractor_handles");

    const int arrow_points = 5;
    Vector3 arrow[arrow_points] = {
        Vector3(0, -0.3, 0),
        Vector3(0, -0.8, 0),
        Vector3(-1, 0, 0),
        Vector3(0, 0.8, 0),
        Vector3(0, 0.3, 0),
    };

    // creation of 2d quarter of gizmo
    Vector<Vector3> arrow_lines;

    float radius = attractor->get_range();
    float strength = attractor->get_strength();
    float mult = ( abs( strength ) <= 1 ) ? 1 : sqrt(abs( strength ));

    float str = strength;
    if (strength > 0) {
        str = std::min(strength, radius - mult);
        if (str < 0) {
            str = 0;
        }
    }
    // angle of the arrow basis intersection with circle
    float m = mult;
    float proja = 0;
    float arrow_offset = 0;
    float ratio = mult * arrow[4].y / radius;
    if (ratio > 0.9999) {
        ratio = 0.9999;
    }
    proja = asin(ratio);
    if (strength < 0 && proja > 20 / 180.f * M_PI) {
        proja = 20 / 180.f * M_PI;
        m = abs(sin(proja)) * radius / arrow[4].y;
        arrow_offset = 1 - cos(proja);
    }
    arrow_lines.push_back(Vector3(radius - arrow_offset * radius, arrow[0].y * m, 0));
    float multx = 1;
    if (strength < 0) {
        multx *= -1;
    }
    for (int i = 0; i < 5; ++i) {
        arrow_lines.push_back(Vector3(radius - (str + arrow_offset * radius) + arrow[i].x * m * multx, arrow[i].y * m, 0));
    }
    arrow_lines.push_back(Vector3(radius - arrow_offset * radius, arrow[4].y * m, 0));
    // arc
    float a = proja;
    float agap = ((M_PI / 2) - proja * 2) / 8;
    for (int i = 0; i < 9; ++i) {
        arrow_lines.push_back(Vector3(cos(a) * radius, sin(a) * radius, 0));
        a += agap;
    }


    int alnum = arrow_lines.size();
    Vector<Vector3> lines;
    
    Basis tilt_matrix;
//    tilt_matrix.rotate(Vector3(1, 0, 0), M_PI / 4);
//    tilt_matrix.rotate(tilt_matrix.xform(Vector3(0, 1, 0)), M_PI / 4);

    for (int r = 0; r < 3; ++r) {
        Basis matrix;
        Vector3 axis(0, 0, 1);
        switch (r) {
            case 1:
                matrix.rotate(Vector3(0, 1, 0), M_PI / 2);
                axis = Vector3(1, 0, 0);
                break;
            case 2:
                matrix.rotate(Vector3(1, 0, 0), M_PI / 2);
                axis = Vector3(0, 1, 0);
                break;
        }
        for (int z = 0; z < 4; ++z) {
            matrix.rotate( axis, M_PI / 2);
            for (int i = 0, j = 1; i < alnum - 1; ++i, ++j) {
                j %= alnum;
                lines.push_back(matrix.xform(arrow_lines[i]));
                lines.push_back(matrix.xform(arrow_lines[j]));
            }
        }
    }

    p_gizmo->add_lines(lines, material);

}

String FutariAttractorGizmoPlugin::get_handle_name(const EditorSpatialGizmo *p_gizmo, int p_idx) const {
    if (p_idx == 0)
        return "Range";
    else
        return "Strength";
}

Variant FutariAttractorGizmoPlugin::get_handle_value(EditorSpatialGizmo *p_gizmo, int p_idx) const {

    FutariWind *wind = Object::cast_to<FutariWind>(p_gizmo->get_spatial_node());
    if (p_idx == 0)
        return wind->get_range();
    if (p_idx == 1)
        return wind->get_strength();
    return Variant();

}

void FutariAttractorGizmoPlugin::set_handle(EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point) {

    FutariAttractor *attractor = Object::cast_to<FutariAttractor>(p_gizmo->get_spatial_node());
    Transform gt = attractor->get_global_transform();
    gt.orthonormalize();
    Transform gi = gt.affine_inverse();

    Vector3 ray_from = p_camera->project_ray_origin(p_point);
    Vector3 ray_dir = p_camera->project_ray_normal(p_point);

    Vector3 s[2] = {gi.xform(ray_from), gi.xform(ray_from + ray_dir * 4096)};

    if (p_idx == 0) {

    } else if (p_idx == 1) {

    }

}

void FutariAttractorGizmoPlugin::commit_handle(EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel) {

    FutariAttractor *attractor = Object::cast_to<FutariAttractor>(p_gizmo->get_spatial_node());

    if (p_cancel) {

        if (p_idx == 0) {
            attractor->set_range(p_restore);
        } else if (p_idx == 0) {
            attractor->set_strength(p_restore);
        }

    } else if (p_idx == 0) {

        //        UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
        //        ur->create_action(TTR("Change Light Radius"));
        //        ur->add_do_method(light, "set_param", Light::PARAM_RANGE, light->get_param(Light::PARAM_RANGE));
        //        ur->add_undo_method(light, "set_param", Light::PARAM_RANGE, p_restore);
        //        ur->commit_action();

    } else if (p_idx == 1) {

        //        UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
        //        ur->create_action(TTR("Change Light Radius"));
        //        ur->add_do_method(light, "set_param", Light::PARAM_SPOT_ANGLE, light->get_param(Light::PARAM_SPOT_ANGLE));
        //        ur->add_undo_method(light, "set_param", Light::PARAM_SPOT_ANGLE, p_restore);
        //        ur->commit_action();

    }
}

// +++++++++++++++ VORTEX +++++++++++++++

FutariVortexGizmoPlugin::FutariVortexGizmoPlugin() {

    Color gizmo_color = EDITOR_DEF("editors/3d_gizmos/gizmo_colors/futari_vortex", Color(1.0, 1.0, 0.0, 0.7));
    create_material("futari_vortex", gizmo_color);
    create_handle_material("futari_vortex_handles");

}

bool FutariVortexGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariVortex>(p_spatial);
}

String FutariVortexGizmoPlugin::get_name() const {
    return "FutariVortex";
}

bool FutariVortexGizmoPlugin::is_selectable_when_hidden() const {
    return true;
}

void FutariVortexGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

    std::cout << "FutariVortexGizmoPlugin::redraw" << std::endl;

    FutariVortex *vortex = Object::cast_to<FutariVortex>(p_gizmo->get_spatial_node());

    p_gizmo->clear();

    Ref<Material> material = get_material("futari_vortex", p_gizmo);
    Ref<Material> handles_material = get_material("futari_vortex_handles");

    //    const int arrow_points = 7;
    //    const float arrow_length = 1.5;
    //    float m = wind->get_strength();
    //    if ( m > 1 ) {
    //        m = Math::sqrt( m );
    //    }
    //
    //    Vector3 arrow[arrow_points] = {
    //        Vector3(0, 0, -1 * m),
    //        Vector3(0, 0.5 * m, 0),
    //        Vector3(0, 0.3 * m, 0),
    //        Vector3(0, 0.3 * m, arrow_length * m),
    //        Vector3(0, -0.3 * m, arrow_length * m),
    //        Vector3(0, -0.3 * m, 0),
    //        Vector3(0, -0.5 * m, 0)
    //    };
    //
    //    int arrow_sides = 2;

    Vector<Vector3> lines;

    //    for (int i = 0; i < arrow_sides; i++) {
    //        for (int j = 0; j < arrow_points; j++) {
    //            Basis ma(Vector3(0, 0, 1), Math_PI * i / arrow_sides);
    //
    //            Vector3 v1 = arrow[j] - Vector3(0, 0, arrow_length * m);
    //            Vector3 v2 = arrow[(j + 1) % arrow_points] - Vector3(0, 0, arrow_length * m);
    //
    //            lines.push_back(ma.xform(v1));
    //            lines.push_back(ma.xform(v2));
    //        }
    //    }

    // range sphere
    const float radius = vortex->get_range();
    const float agap = (Math_PI / 16);
    float a = 0;
    Vector<Vector2> circle;
    for (int i = 0; i < 32; ++i) {
        circle.push_back(Vector2(Math::cos(a + agap) * radius, Math::sin(a + agap) * radius));
        a += agap;
    }
    for (int i = 0; i < 3; ++i) {
        for (int j = 0, k = 1; j < 32; ++j, ++k) {
            k %= 32;
            Vector2 v1 = circle[j];
            Vector2 v2 = circle[k];
            switch (i) {
                case 0: // floor
                    lines.push_back(Vector3(v1.x, 0, v1.y));
                    lines.push_back(Vector3(v2.x, 0, v2.y));
                    break;
                case 1: // front wall
                    lines.push_back(Vector3(v1.x, v1.y, 0));
                    lines.push_back(Vector3(v2.x, v2.y, 0));
                    break;
                case 2: // side wall
                    lines.push_back(Vector3(0, v1.x, v1.y));
                    lines.push_back(Vector3(0, v2.x, v2.y));
                    break;
                default:
                    break;
            }
        }
    }

    p_gizmo->add_lines(lines, material);

    //    p_gizmo->add_unscaled_billboard(icon, 0.05);

}

String FutariVortexGizmoPlugin::get_handle_name(const EditorSpatialGizmo *p_gizmo, int p_idx) const {
    if (p_idx == 0)
        return "Range";
    else
        return "Strength";
}

Variant FutariVortexGizmoPlugin::get_handle_value(EditorSpatialGizmo *p_gizmo, int p_idx) const {

    FutariVortex *vortex = Object::cast_to<FutariVortex>(p_gizmo->get_spatial_node());
    if (p_idx == 0)
        return vortex->get_range();
    if (p_idx == 1)
        return vortex->get_barycentre();
    if (p_idx == 2)
        return vortex->get_strength();
    return Variant();

}

void FutariVortexGizmoPlugin::set_handle(EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point) {

    FutariVortex *vortex = Object::cast_to<FutariVortex>(p_gizmo->get_spatial_node());
    Transform gt = vortex->get_global_transform();
    gt.orthonormalize();
    Transform gi = gt.affine_inverse();

    Vector3 ray_from = p_camera->project_ray_origin(p_point);
    Vector3 ray_dir = p_camera->project_ray_normal(p_point);

    Vector3 s[2] = {gi.xform(ray_from), gi.xform(ray_from + ray_dir * 4096)};

    if (p_idx == 0) {

    } else if (p_idx == 1) {

    } else if (p_idx == 2) {

    }

}

void FutariVortexGizmoPlugin::commit_handle(EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel) {

    FutariVortex *vortex = Object::cast_to<FutariVortex>(p_gizmo->get_spatial_node());

    if (p_cancel) {

        if (p_idx == 0) {
            vortex->set_range(p_restore);
        } else if (p_idx == 1) {
            vortex->set_barycentre(p_restore);
        } else if (p_idx == 2) {
            vortex->set_strength(p_restore);
        }

    } else if (p_idx == 0) {

        //        UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
        //        ur->create_action(TTR("Change Light Radius"));
        //        ur->add_do_method(light, "set_param", Light::PARAM_RANGE, light->get_param(Light::PARAM_RANGE));
        //        ur->add_undo_method(light, "set_param", Light::PARAM_RANGE, p_restore);
        //        ur->commit_action();

    } else if (p_idx == 1) {

        //        UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
        //        ur->create_action(TTR("Change Light Radius"));
        //        ur->add_do_method(light, "set_param", Light::PARAM_SPOT_ANGLE, light->get_param(Light::PARAM_SPOT_ANGLE));
        //        ur->add_undo_method(light, "set_param", Light::PARAM_SPOT_ANGLE, p_restore);
        //        ur->commit_action();

    }
}

EditorPluginFutari::EditorPluginFutari(EditorNode *p_editor) {

    Ref<FutariWindGizmoPlugin> futari_wind_plugin = Ref<FutariWindGizmoPlugin>(memnew(FutariWindGizmoPlugin));
    SpatialEditor::get_singleton()->add_gizmo_plugin(futari_wind_plugin);

    Ref<FutariAttractorGizmoPlugin> futari_attractor_plugin = Ref<FutariAttractorGizmoPlugin>(memnew(FutariAttractorGizmoPlugin));
    SpatialEditor::get_singleton()->add_gizmo_plugin(futari_attractor_plugin);

    Ref<FutariVortexGizmoPlugin> futari_vortex_plugin = Ref<FutariVortexGizmoPlugin>(memnew(FutariVortexGizmoPlugin));
    SpatialEditor::get_singleton()->add_gizmo_plugin(futari_vortex_plugin);

}