# Futari addon

Module for [godot engine](https://godotengine.org/) developed for Futari Shizuka, The Maiden from the Sea by Toshio Hosokawa.

Contains classes to interacts with particle systems, such as wind, vortex and localised modifiers.

Developed for latest version of godot engine: requires a recompilation of the engine and invalidates export templates!

## demo project

Check the demo project for this addon here: https://gitlab.com/frankiezafe/futari-addon-demo

![](https://polymorph.cool/wp-content/uploads/2019/01/futari-addon_particles_wind.png)

## compilation

``` bash
    soon
```
