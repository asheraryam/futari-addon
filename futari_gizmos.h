/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_gizmos.h
 * Author: frankiezafe
 *
 * Created on January 9, 2019, 11:36 AM
 */

#ifndef FUTARI_GIZMOS_H
#define FUTARI_GIZMOS_H

#include <iostream>
#include <algorithm>

#include "futari_wind.h"
#include "futari_attractor.h"
#include "futari_vortex.h"
#include "editor/editor_plugin.h"
#include "editor/spatial_editor_gizmos.h"

class FutariWindGizmoPlugin : public EditorSpatialGizmoPlugin {
    GDCLASS( FutariWindGizmoPlugin, EditorSpatialGizmoPlugin )

public:

    bool has_gizmo( Spatial *p_spatial );
    String get_name( ) const;
    bool is_selectable_when_hidden( ) const;

    void redraw( EditorSpatialGizmo *p_gizmo );

    String get_handle_name( const EditorSpatialGizmo *p_gizmo, int p_idx ) const;
    Variant get_handle_value( EditorSpatialGizmo *p_gizmo, int p_idx ) const;
    void set_handle( EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point );
    void commit_handle( EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel );

    FutariWindGizmoPlugin( );

};

class FutariAttractorGizmoPlugin : public EditorSpatialGizmoPlugin {
    GDCLASS( FutariAttractorGizmoPlugin, EditorSpatialGizmoPlugin )

public:

    bool has_gizmo( Spatial *p_spatial );
    String get_name( ) const;
    bool is_selectable_when_hidden( ) const;

    void redraw( EditorSpatialGizmo *p_gizmo );

    String get_handle_name( const EditorSpatialGizmo *p_gizmo, int p_idx ) const;
    Variant get_handle_value( EditorSpatialGizmo *p_gizmo, int p_idx ) const;
    void set_handle( EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point );
    void commit_handle( EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel );

    FutariAttractorGizmoPlugin( );

};

class FutariVortexGizmoPlugin : public EditorSpatialGizmoPlugin {
    GDCLASS( FutariVortexGizmoPlugin, EditorSpatialGizmoPlugin )

public:

    bool has_gizmo( Spatial *p_spatial );
    String get_name( ) const;
    bool is_selectable_when_hidden( ) const;

    void redraw( EditorSpatialGizmo *p_gizmo );

    String get_handle_name( const EditorSpatialGizmo *p_gizmo, int p_idx ) const;
    Variant get_handle_value( EditorSpatialGizmo *p_gizmo, int p_idx ) const;
    void set_handle( EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point );
    void commit_handle( EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel );

    FutariVortexGizmoPlugin( );

};

class EditorPluginFutari : public EditorPlugin {
    GDCLASS( EditorPluginFutari, EditorPlugin )
public:
    EditorPluginFutari( EditorNode *p_editor );
};

#endif /* FUTARI_GIZMOS_H */